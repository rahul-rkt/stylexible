/**
 * Here you define all the fonts and related functions.
 *
 * @author R
 */

import FontFaceObserver from 'fontfaceobserver';

const fonts = {
    // declaration for the standard to be used throughout
    master: {
        weight: 400,
        family: 'Noto Sans',
        fallback:
            'Open Sans, Helvetica Neue, Helvetica, Roboto, Arial, sans-serif'
    },

    // declaration for body or content
    bold: {
        weight: 700,
        family: 'Noto Sans',
        fallback:
            'Open Sans, Helvetica Neue, Helvetica, Roboto, Arial, sans-serif'
    },

    // declaration for all variations required throughout the app
    // NOTE: keep the weights and italics as arrays
    get imports() {
        return [
            {
                family: fonts.master.family,
                weights: [ '400', '700' ],
                italics: [ '400i', '700i' ]
            }
        ];
    },

    // helper for generating import url for google fonts
    get importUrl() {
        let url = 'https://fonts.googleapis.com/css?family=';
        // prettier-ignore
        url += fonts.imports.map(
            font => {
                const family = font.family.replace(/\s+/g, '+');
                // eslint-disable-next-line
                const styles = font.hasOwnProperty('italics')
                    ? font.weights.concat(font.italics).sort()
                    : font.weights;
                return `${family}:${styles}`;
            }).join('|');
        return url;
    },

    // helper for generating FontFaceObserver objects to handle font loading
    get ffos() {
        // prettier-ignore
        const ffos = fonts.imports.map(
            font => {
                const ffoByWeights = font.weights.map(
                    w => new FontFaceObserver(font.family, { weight: +w })
                );
                // eslint-disable-next-line
                const ffoByItalics = font.hasOwnProperty('italics')
                    ? font.italics.map(
                        i => new FontFaceObserver(font.family, {
                            weight: +i.slice(0, -1),
                            style: 'italic'
                        })
                    ) : [];
                return ffoByWeights.concat(ffoByItalics);
            });
        return Array.concat(...ffos);
    }
};

export default fonts;
