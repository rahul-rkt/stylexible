/**
 * These are the global styles which are injected on the html head.
 *
 * @author R
 */

import { injectGlobal } from 'styled-components';
import { colors, fonts, units, mediaQueries, styles } from 'Styles';

// scaffolding
injectGlobal`
    @import url(${fonts.importUrl});

    body {
        hyphens: none;
        user-select: none;
        word-wrap: normal;
        word-break: normal;
        overflow-x: hidden;
        overflow-wrap: normal;
        vertical-align: baseline;

        text-align: left;
        font-smooth: always;
        font-smoothing: antialiased;
        -moz-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        -webkit-font-smoothing: antialiased;
        -webkit-font-smoothing: subpixel-antialiased;
        -webkit-text-stroke: ${units.pxToEm(1)} transparent;
        text-shadow:
            ${units.rem(units.$px)} ${units.rem(units.$px)}
            ${units.rem(units.$px)} rgba(0, 0, 0, 0.004);
        text-rendering: optimizeLegibility;
        text-rendering: geometricPrecision;

        color: ${colors.fg.default};
        background-color: ${colors.bg.default};

        font-family: ${fonts.master.fallback};
        font-feature-settings: "kern" 1, "liga" 1, "pnum" 1;
    }

    .fonts-loaded body {
        font-family: ${fonts.master.family};
    }

    #app {
        width: 100vw;
        min-width: 100vw;
        max-width: 100vw;
        user-select: text;
        color: ${colors.fg.default};
        background-color: ${colors.bg.default};
    }

    h1, h2, h3, h4, h5, h6, header {
        font-family: ${fonts.master.fallback};
    }

    h1, strong, .bold {
        font-weight: ${fonts.bold.weight};
    }

    h2, h3, h4, h5, h6, header, body, .regular {
        font-weight: ${fonts.master.weight};
    }

    .spaced {
        letter-spacing: ${units.spacing.default};
    }

    h1, .heading {
        color: ${colors.fg.heading};
    }

    .fonts-loaded h1, h2, h3, h4, h5, h6, header {
        font-family: ${fonts.master.family};
    }

    img, picture {
        max-width: 100%;
    }

    hr {
        height: 0;
        border: 0;
        border-top: ${units.rem(units.$2px)} solid ${colors.palette.greyBright};
    }

    ${styles.links.default}

    ${mediaQueries.onRetina}
    ${mediaQueries.onDesktop}
    ${mediaQueries.onTablet}
    ${mediaQueries.onMobile}
`;

fonts.ffos.reduce(
    (p = true, ffo) => p && ffo.load().then(s => true, e => false)
) && (document.documentElement.className += ' fonts-loaded');
