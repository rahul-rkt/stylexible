/**
 * This contains all the defined units and helpers to build with them.
 *
 * @author R
 */

import ratios from './_3_ratios';

const units = {
    // base units
    $em: 1,
    $px: 0.0625,
    $2px: 0.125,
    $lineHeights: {
        onRetina: 1.6,
        onDesktop: 1.5,
        onTablet: 1.4,
        onMobile: 1.3
    },

    // borders
    borders: {
        get size() {
            return units.rem(units.$px);
        },
        get radius() {
            return units.rem(units.$2px);
        }
    },

    // letter-spacing
    spacing: {
        get default() {
            return '0.15vw';
        },
        get large() {
            return '0.3vw';
        }
    },

    // unit helpers
    roundOff(value) {
        return Math.round(value * 100) / 100;
    },
    unit(value, unit) {
        return `${units.roundOff(value)}${unit}`;
    },
    px(value) {
        return units.unit(value, 'px');
    },
    em(value) {
        return units.unit(value, 'em');
    },
    rem(value) {
        return units.unit(value, 'rem');
    },
    pc(value) {
        return units.unit(value, '%');
    },
    pxToEm(value) {
        return units.em(value * units.$px);
    },
    pxToRem(value) {
        return units.rem(value * units.$px);
    },
    emToPx(value) {
        return units.px(value / units.$px);
    },

    // scaling parameters
    scales: {
        onRetina: {
            get base() {
                return [ units.$em + 0.3, units.$em * ratios.minorThird + 0.3 ];
            },
            get ratio() {
                return ratios.augmentedFourth;
            }
        },
        onDesktop: {
            get base() {
                return [
                    units.$em + 0.2,
                    units.$em * ratios.majorerSecond + 0.2
                ];
            },
            get ratio() {
                return ratios.perfectFourth;
            }
        },
        onTablet: {
            get base() {
                return [ units.$em + 0.1, units.$em * ratios.majorSecond + 0.1 ];
            },
            get ratio() {
                return ratios.majorThird;
            }
        },
        onMobile: {
            get base() {
                return [ units.$em, units.$em * ratios.minorSecond ];
            },
            get ratio() {
                return ratios.majorSecond;
            }
        }
    },

    // generates the complete set of scaled units for defined scales
    get scaledUnits() {
        const scaledUnits = {};
        Object.entries(units.scales).map(
            // prettier-ignore
            ([ key, scale ]) => (scaledUnits[key] = ratios.scaleGenerator(scale))
        );
        return scaledUnits;
    },

    // line height calculator
    calcLineHeight(fontSize, baseLineHeight) {
        return units.roundOff(
            Math.ceil(fontSize / baseLineHeight) * (baseLineHeight / fontSize)
        );
    },

    // top-right-bottom-left notation support
    trbl(...trbl) {
        switch (trbl.length) {
            case 1:
                return `${trbl[0]} ${trbl[0]} ${trbl[0]} ${trbl[0]}`;
            case 2:
                return `${trbl[0]} ${trbl[1]} ${trbl[0]} ${trbl[1]}`;
            case 3:
                return `${trbl[0]} ${trbl[1]} ${trbl[2]} ${trbl[1]}`;
            case 4:
                return `${trbl[0]} ${trbl[1]} ${trbl[2]} ${trbl[3]}`;
            default:
                throw Error('Invalid Argument Exception');
        }
    }
};

export default units;
