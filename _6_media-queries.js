/**
 * Here you define all the prepared media queries.
 *
 * @author R
 */

import units from './_4_units';
import breakpoints from './_5_breakpoints';

const scaledUnits = units.scaledUnits;

const mediaQueries = {
    // media queries
    get onRetina() {
        return mediaQueries.queryBuilder('onRetina');
    },
    get onDesktop() {
        return mediaQueries.queryBuilder('onDesktop');
    },
    get onTablet() {
        return mediaQueries.queryBuilder('onTablet');
    },
    get onMobile() {
        return mediaQueries.queryBuilder('onMobile');
    },

    // the query builder
    queryBuilder(onKey) {
        return `${breakpoints[onKey]} {
        .zetta {
            font-size: ${units.rem(scaledUnits[onKey].zetta)};
            line-height: ${units.calcLineHeight(
                scaledUnits[onKey].zetta,
                units.$lineHeights[onKey]
            )};
        }

        .exa {
            font-size: ${units.rem(scaledUnits[onKey].exa)};
            line-height: ${units.calcLineHeight(
                scaledUnits[onKey].exa,
                units.$lineHeights[onKey]
            )};
        }

        h1, .peta {
            font-size: ${units.rem(scaledUnits[onKey].peta)};
            line-height: ${units.calcLineHeight(
                scaledUnits[onKey].peta,
                units.$lineHeights[onKey]
            )};
        }

        h2, .tera {
            font-size: ${units.rem(scaledUnits[onKey].tera)};
            line-height: ${units.calcLineHeight(
                scaledUnits[onKey].tera,
                units.$lineHeights[onKey]
            )};
        }

        h3, .giga {
            font-size: ${units.rem(scaledUnits[onKey].giga)};
            line-height: ${units.calcLineHeight(
                scaledUnits[onKey].giga,
                units.$lineHeights[onKey]
            )};
        }

        h4, .mega {
            font-size: ${units.rem(scaledUnits[onKey].mega)};
            line-height: ${units.calcLineHeight(
                scaledUnits[onKey].mega,
                units.$lineHeights[onKey]
            )};
        }

        h5, .kilo {
            font-size: ${units.rem(scaledUnits[onKey].kilo)};
            line-height: ${units.calcLineHeight(
                scaledUnits[onKey].kilo,
                units.$lineHeights[onKey]
            )};
        }

        body, h6, .byte {
            font-size: ${units.rem(scaledUnits[onKey].byte)};
            line-height: ${units.calcLineHeight(
                scaledUnits[onKey].byte,
                units.$lineHeights[onKey]
            )};
        }

        .nibble {
            font-size: ${units.rem(scaledUnits[onKey].nibble)};
            line-height: ${units.calcLineHeight(
                scaledUnits[onKey].nibble,
                units.$lineHeights[onKey]
            )};
        }

        .bit {
            font-size: ${units.rem(scaledUnits[onKey].bit)};
            line-height: ${units.calcLineHeight(
                scaledUnits[onKey].bit,
                units.$lineHeights[onKey]
            )};
        }

        h1, h2, h3, h4, h5, h6,
        p, ul, ol, dl, dd, figure,
        blockquote, details, hr,
        fieldset, pre, table {
            margin: 0 0 ${units.rem(units.$lineHeights[onKey])};
        }

        .push-quadruple {
            margin-bottom: ${units.rem(units.$lineHeights[onKey] * 4)};
        }

        .push-triple {
            margin-bottom: ${units.rem(units.$lineHeights[onKey] * 3)};
        }

        .push-double {
            margin-bottom: ${units.rem(units.$lineHeights[onKey] * 2)};
        }

        .push {
            margin-bottom: ${units.rem(units.$lineHeights[onKey])};
        }

        .push-half {
            margin-bottom: ${units.rem(units.$lineHeights[onKey] * 0.5)};
        }

        .push-quarter {
            margin-bottom: ${units.rem(units.$lineHeights[onKey] * 0.25)};
        }

        .lift-quadruple {
            margin-top: ${units.rem(units.$lineHeights[onKey] * 4)};
        }

        .lift-triple {
            margin-top: ${units.rem(units.$lineHeights[onKey] * 3)};
        }

        .lift-double {
            margin-top: ${units.rem(units.$lineHeights[onKey] * 2)};
        }

        .lift {
            margin-top: ${units.rem(units.$lineHeights[onKey])};
        }

        .lift-half {
            margin-top: ${units.rem(units.$lineHeights[onKey] * 0.5)};
        }

        .lift-quarter {
            margin-top: ${units.rem(units.$lineHeights[onKey] * 0.25)};
        }

        .buffer-quadruple {
            margin-top: ${units.rem(units.$lineHeights[onKey] * 4)};
            margin-bottom: ${units.rem(units.$lineHeights[onKey] * 4)};
        }

        .buffer-triple {
            margin-top: ${units.rem(units.$lineHeights[onKey] * 3)};
            margin-bottom: ${units.rem(units.$lineHeights[onKey] * 3)};
        }

        .buffer-double {
            margin-top: ${units.rem(units.$lineHeights[onKey] * 2)};
            margin-bottom: ${units.rem(units.$lineHeights[onKey] * 2)};
        }

        .buffer {
            margin-top: ${units.rem(units.$lineHeights[onKey])};
            margin-bottom: ${units.rem(units.$lineHeights[onKey])};
        }

        .buffer-half {
            margin-top: ${units.rem(units.$lineHeights[onKey] * 0.5)};
            margin-bottom: ${units.rem(units.$lineHeights[onKey] * 0.5)};
        }

        .buffer-quarter {
            margin-top: ${units.rem(units.$lineHeights[onKey] * 0.25)};
            margin-bottom: ${units.rem(units.$lineHeights[onKey] * 0.25)};
        }

        .island-quadruple {
            padding: ${units.rem(units.$lineHeights[onKey] * 4)};
        }

        .island-triple {
            padding: ${units.rem(units.$lineHeights[onKey] * 3)};
        }

        .island-double {
            padding: ${units.rem(units.$lineHeights[onKey] * 2)};
        }

        .island {
            padding: ${units.rem(units.$lineHeights[onKey])};
        }

        .island-half {
            padding: ${units.rem(units.$lineHeights[onKey] * 0.5)};
        }

        .island-quarter {
            padding: ${units.rem(units.$lineHeights[onKey] * 0.25)};
        }

        .gutter-quadruple {
            padding-left: ${units.rem(units.$lineHeights[onKey] * 4)};
            padding-right: ${units.rem(units.$lineHeights[onKey] * 4)};
        }

        .gutter-triple {
            padding-left: ${units.rem(units.$lineHeights[onKey] * 3)};
            padding-right: ${units.rem(units.$lineHeights[onKey] * 3)};
        }

        .gutter-double {
            padding-left: ${units.rem(units.$lineHeights[onKey] * 2)};
            padding-right: ${units.rem(units.$lineHeights[onKey] * 2)};
        }

        .gutter {
            padding-left: ${units.rem(units.$lineHeights[onKey])};
            padding-right: ${units.rem(units.$lineHeights[onKey])};
        }

        .gutter-half {
            padding-left: ${units.rem(units.$lineHeights[onKey] * 0.5)};
            padding-right: ${units.rem(units.$lineHeights[onKey] * 0.5)};
        }

        .gutter-quarter {
            padding-left: ${units.rem(units.$lineHeights[onKey] * 0.25)};
            padding-right: ${units.rem(units.$lineHeights[onKey] * 0.25)};
        }
    }`;
    }
};

export default mediaQueries;
