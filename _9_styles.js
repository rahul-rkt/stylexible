/**
 * Here you define all the pre-made styles to readily use throughout the application.
 *
 * @author R
 */

import colors from './_1_colors';
import breakpoints from './_5_breakpoints';
import transitions from './_7_transitions';

const styles = {
    // base link styles
    links: {
        get default() {
            return styles.links.base('default');
        },
        get elements() {
            return styles.links.base('elements');
        },
        get special() {
            return styles.links.base('special');
        },
        base(type) {
            return `
                a {
                    text-decoration: none;
                    color: ${colors.link[type].normal};
                    transition: ${transitions.transitionOn()};
                }

                a:visited {
                    color:  ${colors.link[type].visited};
                }

                ${
                    !colors.link[type].active ? 'a:active, ' : ''
                }, a:focus, a:hover {
                    outline: none;
                    color: ${colors.link[type].hover};
                }
                ${
                    // prettier-ignore
                    colors.link[type].active ? `
                        a:active {
                            color: ${colors.link[type].active};
                        }
                    ` : ''
                }
            `;
        }
    },

    // base container styles
    container: {
        // flexbox systems
        flex: {
            // simple centerd flexbox
            centered: `
                width: 70vw;
                height: 100%;
                display: flex;
                margin-left: auto;
                margin-right: auto;
                flex-direction: column;
                justify-content: center;

                ${breakpoints.onRetina} {
                    width: 60vw;
                }

                ${breakpoints.onTablet} {
                    width: 80vw;
                }

                ${breakpoints.onMobile} {
                    width: 90vw;
                }
            `
        },

        // grid systems
        grid: {
            // non-linear 12 column grid
            nl12c: `
                grid-template-columns:
                    [one-twentieth] 5%
                    [one-eighth] 7.5%
                    [one-fifth] 7.5%
                    [one-fourth] 5%
                    [one-third] 8.4%
                    [half] 16.6%
                    [two-third] 16.6%
                    [three-fourth] 8.4%
                    [four-fifth] 5%
                    [seven-eighth] 7.5%
                    [nineteen-twentieth] 7.5%;
            `,
            // non-linear 8 row grid
            nl8r: `
                grid-template-rows:
                    [five] 5%
                    [fifteen] 10%
                    [thirty] 15%
                    [fifty] 20%
                    [seventy] 20%
                    [eighty-five] 15%
                    [ninety-five] 10%;
            `
        }
    }
};

export default styles;
