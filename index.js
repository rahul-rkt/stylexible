/**
 * This just accumulates the other base directives for easier imports.
 *
 * @author R
 */

import colors from './_1_colors';
import fonts from './_2_fonts';
import ratios from './_3_ratios';
import units from './_4_units';
import breakpoints from './_5_breakpoints';
import mediaQueries from './_6_media-queries';
import transitions from './_7_transitions';
import animations from './_8_animations';
import styles from './_9_styles';

export {
    colors,
    fonts,
    ratios,
    units,
    breakpoints,
    mediaQueries,
    transitions,
    animations,
    styles
};
