/**
 * Here you define the breakpoints and helper functions.
 *
 * @author R
 */

import units from './_4_units';

const breakpoints = {
    // these are when the layouts get broken
    threshold: {
        desktop: 1200,
        tablet: 800,
        mobile: 480
    },

    // assume large sized screen beyond defined desktop threshold
    get onRetina() {
        return breakpoints.minScreen(
            units.pxToRem(breakpoints.threshold.desktop + 1)
        );
    },

    // assume regular sized screen between defined tablet and desktop threshold
    get onDesktop() {
        return breakpoints.screen(
            units.pxToRem(breakpoints.threshold.tablet + 1),
            units.pxToRem(breakpoints.threshold.desktop)
        );
    },

    // assume medium sized screen between defined mobile and tablet threshold
    get onTablet() {
        return breakpoints.screen(
            units.pxToRem(breakpoints.threshold.mobile + 1),
            units.pxToRem(breakpoints.threshold.tablet)
        );
    },

    // assume small sized screen for less than defined mobile threshold
    get onMobile() {
        return breakpoints.maxScreen(
            units.pxToRem(breakpoints.threshold.mobile)
        );
    },

    // for any screen larger than defined tablet threshold
    get onLargeScreens() {
        return breakpoints.minScreen(
            units.pxToRem(breakpoints.threshold.tablet + 1)
        );
    },

    // for any screen smaller than defined tablet threshold
    get onSmallScreens() {
        return breakpoints.maxScreen(
            units.pxToRem(breakpoints.threshold.tablet)
        );
    },

    // helper function for generating min-width media query
    minScreen($minResolution, $orientation = false) {
        return breakpoints.screen($minResolution, false, $orientation);
    },

    // helper function for generating max-width media query
    maxScreen($maxResolution, $orientation = false) {
        return breakpoints.screen(false, $maxResolution, $orientation);
    },

    // main function for generating media query
    screen(minResolution = false, maxResolution = false, orientation = false) {
        if (!minResolution && !maxResolution && !orientation)
            throw Error('Invalid Argument Exception');

        let mediaQuery = '@media screen';

        minResolution = minResolution && `(min-width: ${minResolution})`;
        maxResolution = maxResolution && `(max-width: ${maxResolution})`;
        orientation = orientation && `(orientation: ${orientation})`;

        mediaQuery += minResolution ? ` and ${minResolution}` : '';
        mediaQuery += maxResolution ? ` and ${maxResolution}` : '';
        mediaQuery += orientation ? ` and ${orientation}` : '';

        return mediaQuery;
    }
};

export default breakpoints;
