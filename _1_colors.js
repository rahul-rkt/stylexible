/**
 * Here you define all the colors and related functions.
 *
 * @author R
 */

const colors = {
    palette: {
        // whites
        white: '#FAFAFF',
        whiteBright: '#FFFFFF',
        // blacks
        black: '#17171A',
        blackBright: '#2A2A2F',
        // greys
        grey: '#46494C',
        greyBright: '#E8E8E8',
        // reds
        red: '#F25F5C',
        redBright: '#E63946',
        // greens
        green: '#00A896',
        greenBright: '#02C39A',
        // blues
        blue: '#41ADDD',
        blueBright: '#00A8E8',
        // yellows
        yellow: '#E9C46A',
        yellowBright: '#FFE066',
        // magentas
        magenta: '#9F6D99',
        magentaBright: '#CFBAE1',
        // cyans
        cyan: '#70C1B3',
        cyanBright: '#A5FFD6'
    },

    // brand colors
    brand: {
        blue: '#00A5DD',
        black: 'rgba(0, 0, 0, 0.85)',
        darkgrey: 'rgba(0, 0, 0, 0.65)',
        grey: 'rgba(0, 0, 0, 0.45)',
        lightGrey: '#E8E8E8',
        whiteSmoke: '#F5F5F5',
        white: '#FFFFFF'
    },

    // mask
    get mask() {
        return colors.brand.grey;
    },

    // background colors
    bg: {
        get default() {
            return colors.brand.white;
        },
        get elements() {
            return colors.brand.whiteSmoke;
        },
        get special() {
            return colors.brand.blue;
        },
        get footer() {
            return colors.brand.darkgrey;
        }
    },

    // foreground colors
    fg: {
        get default() {
            return colors.brand.black;
        },
        get elements() {
            return colors.brand.black;
        },
        get special() {
            return colors.brand.white;
        },
        get heading() {
            return colors.brand.blue;
        },
        get footer() {
            return colors.brand.white;
        }
    },

    // anchor colors
    link: {
        default: {
            get normal() {
                return colors.brand.blue;
            },
            get hover() {
                return colors.brand.black;
            },
            get visited() {
                return colors.brand.blue;
            }
        },
        elements: {
            get normal() {
                return colors.brand.black;
            },
            get hover() {
                return colors.brand.blue;
            },
            get visited() {
                return colors.brand.black;
            },
            get active() {
                return colors.brand.blue;
            }
        },
        special: {
            get normal() {
                return colors.brand.white;
            },
            get hover() {
                return colors.brand.whiteSmoke;
            },
            get visited() {
                return colors.brand.white;
            }
        }
    },

    // borders
    borders: {
        get default() {
            return colors.brand.lightGrey;
        }
    },

    // buttons
    buttons: {
        cta: {
            get bg() {
                return colors.brand.blue;
            },
            get fg() {
                return colors.brand.white;
            }
        }
    }
};

export default colors;
