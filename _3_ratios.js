/**
 * This contains all the ratios and modular scale helper.
 *
 * @author R
 */

const ratios = {
    // ratios
    minorSecond: 1.067,
    majorSecond: 1.125,
    majorerSecond: 1.15,
    minorThird: 1.2,
    majorThird: 1.25,
    perfectFourth: 1.333,
    augmentedFourth: 1.414,
    perfectFifth: 1.5,
    minorSixth: 1.6,
    golden: 1.618,
    majorSixth: 1.667,
    minorSeventh: 1.778,
    majorSeventh: 1.875,
    octave: 2,
    majorTenth: 2.5,
    majorEleventh: 2.667,
    majorTwelfth: 3,
    doubleOctave: 4,

    // helper for generating scaled units
    scaleGenerator(scale) {
        return {
            zetta: ratios.modularScale(7, scale),
            exa: ratios.modularScale(6, scale),
            peta: ratios.modularScale(5, scale),
            tera: ratios.modularScale(4, scale),
            giga: ratios.modularScale(3, scale),
            mega: ratios.modularScale(2, scale),
            kilo: ratios.modularScale(1, scale),
            byte: ratios.modularScale(0, scale),
            nibble: ratios.modularScale(-1, scale),
            bit: ratios.modularScale(-2, scale)
        };
    },

    // modular scale function
    modularScale(factor, scale) {
        // eslint-disable-next-line
        let { base, ratio } = scale;

        // fast calc if not multi stranded
        if (!Array.isArray(base) || base.length === 1)
            return ratio ** factor * base;

        // normalize bases
        // lower bounds for base values
        const baseLow = base[0];
        // upper bounds for base values
        const baseHigh = ratio * baseLow;
        base = base
            .map(baseCurrent => {
                // shift up if value too low
                while (baseCurrent / 1 < baseLow / 1)
                    baseCurrent *= ratio;
                // shift down if too high
                while (baseCurrent / 1 >= baseHigh / 1)
                    baseCurrent *= ratio ** -1;
                return baseCurrent;
            })
            .sort();

        // figure out what base to use with modulo
        const fByL = factor / base.length;
        const index = Math.round((fByL - Math.floor(fByL)) * base.length);

        // return
        return ratio ** Math.floor(fByL) * base[index];
    }
};

export default ratios;
