/**
 * Here you define all the animation keyframes.
 *
 * @author R
 */

import { keyframes } from 'styled-components';

const animations = {
    get stayAndFadeOut() {
        return keyframes`
        0% {
            opacity: 1;
        }
        90% {
            opacity: 1;
        }
        100% {
            opacity: 0;
        }
        `;
    },

    get pulse() {
        return keyframes`
            0% {
                opacity: 0.8;
                transform: scale(0.9);
            }
            50% {
                opacity: 1;
                transform: scale(1);
            }
            100% {
                opacity: 0.8;
                transform: scale(0.9);
            }
        `;
    },

    get heartbeat() {
        return keyframes`
            0% {
                opacity: 0.9;
                transform: scale(1);
            }
            5% {
                opacity: 0.95;
                transform: scale(1.1);
            }
            10% {
                opacity: 0.9;
                transform: scale(1);
            }
            15% {
                opacity: 1;
                transform: scale(1.25);
            }
            50% {
                opacity: 0.9;
                transform: scale(1);
            }
            100% {
                opacity: 0.9;
                transform: scale(1);
            }
        `;
    }
};

export default animations;
